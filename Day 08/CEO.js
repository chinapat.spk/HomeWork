const {Employee} = require('./Employee')
const {Programmer} = require('./Programmer')
const fs = require('fs')

class CEO extends Employee {
    constructor(firstname, lastname, salary) {
        super(firstname, lastname, salary);
        this.dressCode = 'suit';
        // this.employeesRaw = this.myReadFile('homework1.json')
        let self = this
        this.employeeRaw = self.myReadFile('homework1.json')
        this.employees = [];
    }
    getSalary() { // simulate public method
        return super.getSalary() * 2;
    };
    work(employee) { // simulate public method
        this._fire(employee);
        this._hire(employee);
        this._seminar();
        this._golf();
    }
    increaseSalary(employee, newSalary) {
        employee.setSalary(newSalary)
    }
    _golf() { // simulate private method
        this.dressCode = 'golf_dress';
        console.log("He goes to golf club to find a new connection. Dress with :" + this.dressCode);
    };
    gossip(employee, quote) {
        console.log(`Hey ${employee._firstname}, ${quote}`)
    }
    _fire(employee) {
        this.dressCode = 'tshirt'
        console.log(`${employee._firstname} has been fired! Dress with : ${this.dressCode}`)
    }
    _hire(employee) {
        this.dressCode = 'tshirt'
        console.log(`${employee._firstname} has been hired back! Dress with : ${this.dressCode}`)
    }
    _seminar() {
        this.dressCode = 'suit'
        console.log(`He is going tp seminar Dress with :${this.dressCode}`)
    }
    readJSON(fileName) {
        return new Promise((resolve, reject) => {
            fs.readFile(fileName, 'utf8', (err, data) => {
                data = JSON.parse(data)
                err ? reject(err) : resolve( data )
            })
        })
    }
    async myReadFile(fileName) {
        try {
            let result = []
            let data = await this.readJSON(fileName)
            for(let i = 0; i < data.length ; i++) {
                result[i] = new Programmer (
                    data[i].firstname, 
                    data[i].lastname, data[i].salary, 
                    data[i].id, 
                    data[i].programmerType = 'Full Stack'
                )
            }
            this.employees = result
            return this.employees
        } catch(err) {
            console.error(err)
        }
    }
}

exports.CEO = CEO