const {Employee} = require('./Employee.js')

class Programmer extends Employee {
    constructor (firstname , lastname , salary , id , type) {
        super ( firstname , lastname , salary )
        this._id = id
        this.programmerType = type
    }
    hello() {
        console.log(`Hi, ${this.firstname} let's Code!`)
    }
    createWebsite() {
        console.log(`Creating Website!`)
    }
    fixPC() {
        console.log(`Could you please switch the PC on !`)
    }
    installWindow() {
        console.log(`WTF!! Why should I doing this ??`)
    }
    work() {
        createWebsite()
        fixPC()
        installWindow()
    }
}

exports.Programmer = Programmer