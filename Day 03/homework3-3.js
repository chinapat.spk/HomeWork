const fs = require('fs')

let students =
    [
        { "id": "1001", 'firstname': 'Luke', 'lastname': 'Skywalker' },
        { "id": "1002", 'firstname': 'Tony', 'lastname': 'Stark' },
        { "id": "1003", 'firstname': 'Somchai', 'lastname': 'Jaidee' },
        { "id": "1004", 'firstname': 'Monkey D', 'lastname': 'Luffee' },
    ];
let company = [
    { "id": "1001", "company": "Walt Disney" },
    { "id": "1002", "company": "Marvel" },
    { "id": "1003", "company": "Love2work" },
    { "id": "1004", "company": "One Piece" },
];
let salary = [
    { "id": "1001", "salary": "40000" },
    { "id": "1002", "salary": "1000000" },
    { "id": "1003", "salary": "20000" },
    { "id": "1004", "salary": "9000000" },
];
let like = [
    { "id": "1001", "like": "apple" },
    { "id": "1002", "like": "banana" },
    { "id": "1003", "like": "orange" },
    { "id": "1004", "like": "papaya" },
];
let dislike = [
    { "id": "1001", "dislike": "banana" },
    { "id": "1002", "dislike": "orange" },
    { "id": "1003", "dislike": "papaya" },
    { "id": "1004", "dislike": "apple" },
];

// for merging all keys into an Array then transfer to JSON.stringify
let merge = function (students, company, salary, like, dislike) {
    return new Promise(function (resolve, reject) {
        try {
            let Arr = [];
            for (let i in students) {
                let Obj = Object.assign({}, students[i], company[i], salary[i], like[i], dislike[i])
                Arr.push(Obj)
            }
            let employeesDatabase = JSON.stringify(Arr)
            resolve(employeesDatabase)
        } catch (err) {
            reject(err)
        }
    })
}
// for write a File.json    ** easy part **
let writeJSON = function (data) {
    return new Promise(function (resolve, reject) {
        fs.writeFile('homework3-3.json', data, 'utf8', function (err) {
            if (err)
                reject(err)
            else
                resolve('Write Complete')
        })
    })
}

async function executeAll() {
    try {
        let data = await merge(students, company, salary, like, dislike)
        await writeJSON(data)
        console.log('Done!')
    } catch (err) {
        console.error(err)
    }
}

executeAll();







/*
let Arr = []
let obj = {}
students.forEach(function(employees) {
    // console.log(employees.id)
    Arr.push(employees)
})
function merge(par1, par2) {
    let merged = {}
    for(let i in par1) {
        let P1 = par1[i], P2 = par2[i]
        if(P1.id == P2.id) {
            delete P2.id
            for(let keys in P1) { merged[keys] = P1[keys]}
            for(let keys in P2) { merged[keys] = P2[keys]}
        }
    }
    return merged
}
console.log(merge(Arr, company))
*/

