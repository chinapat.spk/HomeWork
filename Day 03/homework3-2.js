const fs = require('fs')

let head = new Promise(function (resolve, reject) {
    fs.readFile('head.txt', 'utf8', function (err, dataHead) {
        if (err)
            reject(err)
        else
            resolve(dataHead)
    })
})

let body = new Promise(function (resolve, reject) {
    fs.readFile('body.txt', 'utf8', function (err, dataBody) {
        if (err)
            reject(err)
        else
            resolve(dataBody)
    })
})

let leg = new Promise(function (resolve, reject) {
    fs.readFile('leg.txt', 'utf8', function (err, dataLeg) {
        if (err)
            reject(err)
        else
            resolve(dataLeg)
    })
})

let feet = new Promise(function (resolve, reject) {
    fs.readFile('feet.txt', 'utf8', function (err, dataFeet) {
        if (err)
            reject(err)
        else
            resolve(dataFeet)
    })
})

let Robot = function (data) {
    //  let fixedData = data.join('\n')
    data = data.toString()
    let fixedData = data.replace(/,/g, '\n')
    return new Promise(function (resolve, reject) {
        fs.writeFile('robotAsync.txt', fixedData, 'utf8', function (err) {
            if (err)
                reject(err)
            else
                resolve(console.log('Complete!'))
        })
    })
}

async function asyncCopyFile() {
    try {
        let headData = await head
        let bodyData = await body
        let legData = await leg
        let feetData = await feet
        let allData = (headData+'\n'+bodyData+'\n'+legData+'\n'+feetData)
        await Robot(allData)
    } catch (err) {
        console.error(err)
    }
}

asyncCopyFile()