const fs = require('fs')


const readJSON = new Promise(function (resolve, reject) {
    fs.readFile('homework1-4.json', 'utf8', function (err, rawData) {
        let data = JSON.parse(rawData)
        let Obj = {}
        data.forEach(function (items) {
            keys = Object.keys(items)
            keys.forEach(function (item) {
                if (item === 'name') {
                    Obj[item] = items[item]
                }
                if (item === 'gender') {
                    Obj[item] = items[item]
                }
                if (item === 'company') {
                    Obj[item] = items[item]
                }
                if (item === 'email') {
                    Obj[item] = items[item]
                }
                if (item === 'friends') {
                    Obj[item] = items[item]
                }
            })
            if (err)
                reject(err)
            else {
                console.log(Obj)
                resolve(Obj)
            }
        })
    })
})

/*
const writeJSON = function(data) {
    return new Promise(function (resolve, reject) {
        data = JSON.stringify(data)
        // console.log(typeof(data))
        fs.writeFile('homework3-4.json', data , 'utf8', function(err) {
            if(err)
            reject(err)
            else
            resolve()
        })
    })
}


async function execAll() {
    try {
        let data = await readJSON
        await writeJSON(data)
        console.log('Done!')
    } catch (err) {
        console.error(err)
    }
}

execAll()
*/

