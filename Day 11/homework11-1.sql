create database bookstore;
use bookstore;

create table staffs (
    id int auto_increment primary key,
    firstname varchar(255) not null,
    lastname varchar(255) not null,
    age int not null,
    created_at timestamp default now()
);

create table books (
    isbn int primary key,
    name varchar(255) not null,
    price int not null,
    created_at timestamp default now()
);

create table sales_record (
    isbn int not null,
    staffname varchar(255) not null,
    price int not null,
    amount int not null,
    sold_at timestamp default now()
);