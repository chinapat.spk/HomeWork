show databases;
use employees;
show tables;

select distinct title from titles;

select  sum(gender = 'M') as total_male, 
        sum(gender = 'F') as total_female,
        count(*) as total_staff from employees;

select count(distinct last_name) from employees;
