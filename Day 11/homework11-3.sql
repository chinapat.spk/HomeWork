use bookstore;

insert into books (isbn, name, price) 
 values (100003001, 'Math 101', 150),
        (100001002, 'Math 102', 180),
        (100004001, 'Biology 101', 200),
        (100004002, 'Biology 102', 250),
        (100001001, 'Computer Science 101', 590),
        (100001002, 'Computer Science 102', 690),
        (100002001, 'Network Security', 990),
        (100005001, 'Basic Javascipt', 190),
        (100007001, 'Data Structure', 890),
        (200001001, 'Craft Beer', 550);

select * from books where name like '%script';

select * from books where name like '%a%' limit 4;

insert into sales_record ( isbn, staffname, price, amount ) 
 values (100001001, 'iBank', 590, 2),
        (100001002, 'iBank', 690, 2),
        (200001001, 'Mook', 550, 2),
        (100004001, 'pSanti', 200, 1),
        (100002001, 'iBank', 990, 2),
        (100007001, 'Danny', 890, 2),
        (100001002, 'Ben', 690, 1),
        (200001001, 'iBank', 550, 5),
        (100001002, 'pSanti', 180, 2),
        (100003001, 'Noob', 150, 1),
        (100007001, 'nPoom', 890, 3),
        (100004001, 'Ben', 200, 2),
        (100001001, 'Noob', 180, 1),
        (100004001, 'nPoom', 200, 3),
        (100002001, 'nPoom', 990, 1),
        (100002001, 'iBank', 990, 2),
        (200001001, 'iBank', 550, 5),
        (100007001, 'Mook', 890, 2),
        (100001002, 'nPoom', 180, 2),
        (100004002, 'Mook', 250, 1),
        (100001001, 'Ben', 590, 1);

select sum(amount) as total from sales_record;

select distinct isbn from sales_record order by isbn;

select sum(price * amount) as Total_Sales from sales_record;