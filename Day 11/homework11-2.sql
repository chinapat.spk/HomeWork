use bookstore;

insert into staffs (firstname, lastname, age) values('iBank', 'Chinapat', 31);
insert into staffs (firstname, lastname, age) values('nPoom', 'theWizard', 16)
insert into staffs (firstname, lastname, age) values('pSanti', 'atLeft', 48);
insert into staffs (firstname, lastname, age) values('Jimmy', 'atRight', 30);
insert into staffs (firstname, lastname, age) values('Will', 'Retire', 59)
insert into staffs (firstname, lastname, age) values('Ben', 'Sorum', 35);
insert into staffs (firstname, lastname, age) values('Danny', 'Sorum', 33);
insert into staffs (firstname, lastname, age) values('Golffy', 'Dhasan', 38);
insert into staffs (firstname, lastname, age) values('Mook', 'Sunisa', 26);
insert into staffs (firstname, lastname, age) values('Noob', 'S', 19);

delete from staffs where id = 5;

alter table staffs add column address varchar(255);

select count(*) from staffs;

select id, firstname from staffs where age < 20;

