const fs = require('fs')

const readJSON = new Promise((resolve, reject) => {
    fs.readFile('homework1-4.json', 'utf8', (err, fileData) => {
        let data = JSON.parse(fileData)
        err ? reject(err) : resolve(data);
    })
})

function sexFilter(data) {
    return data.gender == 'male' 
}
function friendFilter(data) {
    return data.friends.length >= 2
}
const cutField = (data) => {
    let obj = {}
    obj.name = data.name
    obj.gender = data.gender
    obj.company = data.company
    obj.email = data.email
    obj.friends = data.friends
    obj.balance = '$'+(data.balance.replace(',' , '').replace('$',''))/10
    return obj
}

async function execAll() {
    try {
        const arr = await readJSON
        let result = arr.filter(sexFilter)
        .filter(friendFilter)
        .map(cutField)
        console.log(result)
    } catch (err) {
        console.log(err)
    }
}
execAll()
