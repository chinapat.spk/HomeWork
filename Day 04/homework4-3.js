const fs = require('fs')

const readJSON = new Promise((resolve, reject) => {
    fs.readFile('homework1.json', 'utf8', (err, fileData) => {
        let data = JSON.parse(fileData)
        err ? reject(err) : resolve(data);
    })
})

function findLessSalary(input) {
    // let dummy = {...input}
    return (input.salary < 1e5)
}
function multiSalary(input) {
    input.salary *= 2
    return input
}

const sumSalary = ((sum, input) => {
    return parseInt(sum)+parseInt(input.salary)
})

async function exec() {
    try {
        const arr = await readJSON
        let filtered = arr.filter(findLessSalary).map(multiSalary)
        let sum = arr.reduce(sumSalary, 0)
        
        console.log(filtered)
        console.log(sum)
    } catch (err) {
        console.log(err)
    }
}
exec()