const fs = require('fs')

const readJSON = new Promise((resolve, reject) => {
    fs.readFile('homework1.json', 'utf8', (err, fileData) => {
        const data = JSON.parse(fileData)
        // err ? reject(err) : resolve(data);
        if (err) reject(err)
        else {
            resolve(data)
        }
    })
})

const mergeKey = (item) => {
    item.fullName = `${item.firstname}  ${item.lastname}`
    return item
}

function multiSalary(input) {
    let result = []
    let temp = input.salary
    result.push(input.salary)
    result.push(Math.round(temp * 1.1))
    temp = temp * 1.1
    result.push(Math.round(temp * 1.1))
    input.salary = result
    return input
}


async function exec() {
    try {
        let arr = await readJSON
        let merged = arr.map(mergeKey)
        .map(multiSalary)
        console.log(merged)
    } catch (err) {
        console.log(err)
    }
}
exec()