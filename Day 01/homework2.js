// const peopleSalaryRaw = require('./homework1.json');
// console.log(peopleSalary)

const peopleSalary = [
    {
        "id": 1001,
        "firstname": "Luke",
        "lastname": "Skywalker",
        "company": "Walt Disney",
        "salary": 40000
    },
    {
        "id": 1002,
        "firstname": "Tony",
        "lastname": "Stark",
        "company": "Marvel",
        "salary": 1000000
    },
    {
        "id": 1003,
        "firstname": "Somchai",
        "lastname": "Jaidee",
        "company": "Love2work",
        "salary": 20000
    },
    {
        "id": 1004,
        "firstname": "Monkey D",
        "lastname": "Luffee",
        "company": "One Piece",
        "salary": 9000000
    }
]


function removeCompany(Raw) {
    let result = [];
    for (let ArrangePeople in Raw) {
        let cutCompany = Raw[ArrangePeople]
        for (let key in cutCompany) {
            if (key == 'company') {
                delete cutCompany[key]
            }
        }
        result.push(cutCompany)
    }
    return result;
}

console.log(removeCompany(peopleSalary))