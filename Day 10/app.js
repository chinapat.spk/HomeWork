const Koa = require('koa')
let app = new Koa();
// const logger = require('koa-logger')
const render = require('koa-ejs')
const path = require('path')
const {db} = require('./lib/db.js')

db.connectDB();

const middlewares = require('./controller/routes.js')(app);

render(app, {
    root: path.join(__dirname, 'views'),
    layout: 'template',     //  Filename
    viewExt: 'ejs',         //  นามสกุล
    cache: false,
    debug: true
});
