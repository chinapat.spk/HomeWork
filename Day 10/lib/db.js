const {database} = require('../config/database.js')
class Database {
    constructor() {
        this.host = database.host;
        this.user = database.user;
        this.database = database.database;
    }
    async connectDB() {
        const mysql = require('mysql2/promise')
        this.connection = await mysql.createConnection({
            host: this.host,
            user: this.user,
            database: this.database
        })
    }
    execute(sql) {
        return this.connection.execute(sql)
    }
}

module.exports.db = new Database();