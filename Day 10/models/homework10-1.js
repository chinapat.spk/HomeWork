const {db} = require('../lib/db.js')

function getUser() {
    return db.execute(`SELECT * FROM users`)
}
exports.getUser = getUser;