// const {db} = require('../lib/db.js')
const logger = require('koa-logger')
const exe = require('../models/homework10-1.js')

module.exports = function (app) {
    app.use(logger());
    app.use(async(ctx, next) => {
        try {
            // const results = await db.execute(`SELECT * FROM users`)
            const [results, fields] = await exe.getUser()
            await ctx.render('user', {
                'message': results
            });
            await next();
        } catch (err) {
            ctx.status = 400
            ctx.body = `Uh-oh: ${err.message}`
        }
    })
    
    app.listen(3000)
}