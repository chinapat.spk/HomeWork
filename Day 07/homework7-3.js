const input1 = [1, 2, 3]
const input2 = {
    a: 1,
    b: 2
}
const input3 = [1, 2, {
    a: 1,
    b: 2
}]
const input4 = [1, 2, {
    a: 1,
    b: {
        c: 3,
        d: 4
    }
}]

/*
function clone(data) {
    if(Array.isArray(data)) {
        return data.map(clone)
    } else if (data.constructor === Object) {
        return Object.entries(data).map( ([key,value]) => ({
            [key] : clone( value )
        })) .reduce ( (prev , current) => ({ ...prev , ...current }) )
    }
    return data
}
*/
function sepObj(input) {
    let temp = {}
    let keys = Object.keys(input)
    let values = Object.values(input)
    for (let i in keys) {
        if (values[i].constructor !== Object) {
            temp[keys[i]] = values[i]
        } else {
            temp[keys[i]] = sepObj(values[i])
        }
    }
    return temp
}

function sepArr(data) {
    let newArr = []
    data.map(first => {
        // console.log(first)

        if (first.constructor === Object) {
            newArr.push(sepObj(first))
        } else {
            newArr.push(first)
        }
    })
    return newArr
}

function sepElse(data) {
    let temp = data
    return temp
}

function all(input) {
    if (input.constructor === Array) {
        return sepArr(input)
    } else if (input.constructor === Object) {
        return sepObj(input)
    } else {
        return input
    }
}

let arr = all(input4)

arr[2].b.d = 8
console.log(input4)
console.log(arr)






// function seperate(data) {
//     let result = []
//     data.map( each => {
//         if(each.constructor !== Object && each.constructor !== Array) {
//             result.push( sepElse(each) )
//         } else if( each.constructor === Array ) {
//             // sepArr( each )
//         } else if( each.constructor === Object ) {
//             let key = Object.keys( each )
//             let value = Object.values( each )
//             sepObj( keys , values )
//         }
//     })
//     return result
// }