const fs = require('fs')

const read = function (filename , callback) {
    fs.readFile(filename, 'utf8', (err,data) => {
        if(err) console.log(err)
        else {
            data = JSON.parse(data)
            callback(null ,data)
        }
    })
}

function addYearSalary(data) {
    if(data.salary) {
        let yearSalary = data.salary * 12
        return yearSalary
    } else console.error('Error')
}

function addNextSalary (data) {
    let nextSalary = []
    if(data.salary) {
        nextSalary.push(data.salary)
        nextSalary.push(parseInt(data.salary*1.1))
        nextSalary.push(parseInt( (data.salary*1.1)*1.1))
        return nextSalary
    } else console.error('Error')
}

function addAdditionalFields (data) {
    data.yearSalary = addYearSalary(data)
    data.nextSalary = addNextSalary(data)
}

read('./homework1.json', (err, employees) => {
    if(err) console.error(err)
    else employees.map(employee => { 
        addAdditionalFields(employee)
        console.log(employee)
    })
})


/*

const fs = require('fs')

const read = JSON.parse( fs.readFileSync('./homework1.json') )

function addYearSalary(data) {
    if(data.hasOwnProperty('salary') == true ) {
        data.yearSalary = data.salary * 12
        return data.yearSalary
    } else console.error('Error')
}

function addNextSalary (data) {
    let nextSalary = []
    if(data.hasOwnProperty('salary')) {
        nextSalary.push(data.salary)
        nextSalary.push(parseInt(data.salary*1.1))
        nextSalary.push(parseInt( (data.salary*1.1)*1.1))
        return nextSalary
    } else console.error('Error')
}

function addAdditionalFields (data) {
    data.yearSalary = addYearSalary(data)
    data.nextSalary = addNextSalary(data)
    
}


// executor
read.map(function(employee) {
    addAdditionalFields(employee)
    console.log(employee)
})

*/
