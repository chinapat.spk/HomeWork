const fs = require('fs')

const employees = JSON.parse( fs.readFileSync('./homework1.json') )

function addYearSalary(data) {
    if(data.salary) {
        data.yearSalary = data.salary * 12
        return data.yearSalary
    } else console.error('Error')
}

function addNextSalary (data) {
    let nextSalary = []
    if(data.salary) {
        nextSalary.push(data.salary)
        nextSalary.push(parseInt(data.salary*1.1))
        nextSalary.push(parseInt( (data.salary*1.1)*1.1))
        return nextSalary
    } else console.error('Error')
}

function addAdditionalFields (data) {
    let dummy = data.map( employee => {
        return {...employee}
    })
    let result = dummy.map( dum => {
        addYearSalary(dum)
        dum.nextSalary = addNextSalary(dum)
        return dum
    })
    return result
}

let exactData = employees
let currentData = addAdditionalFields(employees)
currentData[3].salary = 0
console.log(exactData)
console.log(currentData)