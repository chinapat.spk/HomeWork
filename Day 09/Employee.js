class Employee {
    constructor(firstname, lastname, salary, id) {
        this._salary = salary; // simulate private variable
        this._firstname = firstname
        this._lastname = lastname
        this._id = id
        const self = this;
    }
    setSalary(newSalary) { // simulate public method
        if(newSalary > this._salary) {
            this._salary = newSalary
            console.log(`${this._firstname}'s salary has been set to ${newSalary}`)
            return newSalary
        } else {
            console.log(`${this._firstname}'s salary is less than before!!`)
            return false
        }
        // return newSalary ถ้ามีเงินเดือนใหม่มีค่ามากกว่า this._salary
        // return false ถ้าเงินเดือนใหม่มีค่าน้อยกว่าเท่ากับ this._salary
    }
    getSalary () {  // simulate public method
        return this._salary;
    };
    work(employee) {
        // leave blank for child class to be overidden
    }
    leaveForVacation(year, month, day) {

    }
}

exports.Employee = Employee