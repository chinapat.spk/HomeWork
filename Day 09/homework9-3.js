const _ = require('lodash')

class MyUtility {
    assign( object , source ) {
        return _.assign(object, source)
    }
    // Same as
    // assign () { return _.assign(...arguments) }

    times( integer ) {
        return _.times( integer )
    }
    keyBy( collection ) {
        return _.keyBy( collection )
    }
    cloneDeep ( input ) {
        return _.cloneDeep ( input )
    }
    filter(collection) {
        return _.filter(collection)
    }
    sortBy(collection) {
        return _.sortBy(collection)
    }
}


const __ = new MyUtility();
let a = { 'A' : 1 , 'B' : 2 }
let b = { 'C' : 9 }
let data = __.assign(a , b)

console.log( data )
