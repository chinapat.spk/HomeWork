const {Employee} = require('./Employee.js')

class Programmer extends Employee {
    constructor (firstname , lastname , salary , id , type) {
        super ( firstname , lastname , salary, id )
        this.type = type
        let self = this
    }
    hello() {
        console.log(`Hi, ${this.firstname} let's Code!`)
    }
    createWebsite() {
        console.log(`Creating Website!`)
    }
    fixPC() {
        console.log(`Could you please switch the PC on !`)
    }
    installWindow() {
        console.log(`WTF!! Why should I doing this ??`)
    }
    work() {
        this.createWebsite()
        this.fixPC()
        this.installWindow()
        // console.log(`Programmer DONE!`)
    }
    talk(message) {
        console.log(message)
    }
    reportRobot( self, robotMessege) {
        self.talk(robotMessege)
    }
}

exports.Programmer = Programmer