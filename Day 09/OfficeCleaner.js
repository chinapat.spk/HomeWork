const {Employee} = require('./Employee.js')

class OfficeCleaner extends Employee {
    constructor ( firstName, lastName, salary, id, dressCode ) {
        super( firstName, lastName, salary, id )
        this.dressCode = dressCode
    }
    work() {
        this.clean()
        this.killBugs()
        this.decorateRoom()
        this.welcomeGuest()
        // console.log(`Maid DONE!`)
    }
    clean() { console.log(`Cleaning`) }
    killBugs() {console.log(`Kill'em All!!`)}
    decorateRoom() {console.log(`Decorating`)}
    welcomeGuest() {console.log(`Welcome`)}
    talk(message) {
        console.log(message)
    }
    reportRobot( self, robotMessege) {
        self.talk(robotMessege)
    }
}

exports.Maid = OfficeCleaner