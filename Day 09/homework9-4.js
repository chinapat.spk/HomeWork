const {CEO} = require('./CEO.js')
const {Employee} = require('./Employee.js')
const {Programmer} = require('./Programmer.js')
const {Maid} = require('./OfficeCleaner.js')

const fs = require('fs')
const EventEmitter = require('events')
const myEmitt = new EventEmitter()

let iBank = new CEO('iBank', 'Chinapat', 9e8, 101, 'Taxedo')

myEmitt.on (`let's talk!` , iBank.reportRobot)

fs.readFile('robot.txt', 'utf8', function (err, data) {
    myEmitt.emit(`let's talk!`, iBank , data)
})
